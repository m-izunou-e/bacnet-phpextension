/*
  +----------------------------------------------------------------------+
  | PHP Version 7                                                        |
  +----------------------------------------------------------------------+
  | Copyright (c) 1997-2018 The PHP Group                                |
  +----------------------------------------------------------------------+
  | This source file is subject to version 3.01 of the PHP license,      |
  | that is bundled with this package in the file LICENSE, and is        |
  | available through the world-wide-web at the following url:           |
  | http://www.php.net/license/3_01.txt                                  |
  | If you did not receive a copy of the PHP license and are unable to   |
  | obtain it through the world-wide-web, please send a note to          |
  | license@php.net so we can mail you a copy immediately.               |
  +----------------------------------------------------------------------+
  | Author:                                                              |
  +----------------------------------------------------------------------+
*/

/* $Id$ */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "php.h"
#include "php_ini.h"
#include "ext/standard/info.h"
#include "php_bacnet.h"

#include "bacdef.h"
#include "bacaddr.h"
#include "bactext.h"
//#include "indtext.h"

#include "include/bacnet_utility.h"
#include "include/whois.h"
#include "include/readprop.h"
#include "include/readrange.h"

#define BAC_ADDRESS_MULT 1

/* If you declare any globals in php_bacnet.h uncomment this:
ZEND_DECLARE_MODULE_GLOBALS(bacnet)
*/

/* True global resources - no need for thread safety here */
//static int le_bacnet;

ZEND_BEGIN_ARG_INFO_EX(arginfo_bacnet_create, 0, 0, 0)
    ZEND_ARG_INFO(0, port)
    ZEND_ARG_INFO(0, bbmd_port)
    ZEND_ARG_INFO(0, bbmd_address)
ZEND_END_ARG_INFO()


/* クラスの変数をGlobalに定義する */
zend_class_entry *bacnet_class_entry;

static void bacnet_ctor(INTERNAL_FUNCTION_PARAMETERS)
{
    zend_string *ip_port = NULL, *bbmd_port = NULL, *bbmd_address = NULL;
//    zval *cf = return_value;
    zval *cf = getThis();

    ZEND_PARSE_PARAMETERS_START(0,3)
        Z_PARAM_OPTIONAL
        Z_PARAM_PATH_STR(ip_port)
        Z_PARAM_STR(bbmd_port)
        Z_PARAM_STR(bbmd_address)
    ZEND_PARSE_PARAMETERS_END();

    if (ip_port) {
        zend_update_property_string(bacnet_class_entry, cf, "port", sizeof("port")-1, ZSTR_VAL(ip_port));
    }

    if (bbmd_port) {
        zend_update_property_string(bacnet_class_entry, cf, "bbmd_port", sizeof("bbmd_port")-1, ZSTR_VAL(bbmd_port));
    }

    if (bbmd_address) {
        zend_update_property_string(bacnet_class_entry, cf, "bbmd_address", sizeof("bbmd_address")-1, ZSTR_VAL(bbmd_address));
    }
}

ZEND_METHOD(Bacnet, __construct)
{
    return_value = getThis();
    bacnet_ctor(INTERNAL_FUNCTION_PARAM_PASSTHRU);
}

PHP_FUNCTION(bacnet_create)
{
    object_init_ex( return_value, bacnet_class_entry );
    bacnet_ctor(INTERNAL_FUNCTION_PARAM_PASSTHRU);
}
/* }}} */



/* {{{ PHP_INI
 */
/* Remove comments and fill if you need to have entries in php.ini
PHP_INI_BEGIN()
    STD_PHP_INI_ENTRY("bacnet.global_value",      "42", PHP_INI_ALL, OnUpdateLong, global_value, zend_bacnet_globals, bacnet_globals)
    STD_PHP_INI_ENTRY("bacnet.global_string", "foobar", PHP_INI_ALL, OnUpdateString, global_string, zend_bacnet_globals, bacnet_globals)
PHP_INI_END()
*/
/* }}} */


/* {{{ php_bacnet_init_globals
 */
/* Uncomment this function if you have INI entries
static void php_bacnet_init_globals(zend_bacnet_globals *bacnet_globals)
{
	bacnet_globals->global_value = 0;
	bacnet_globals->global_string = NULL;
}
*/
/* }}} */






/** クラスメソッドを記述する */
PHP_METHOD(Bacnet, initialize)
{
    zval rv;
    zval *z_ip_port, *z_bbmd_port, *z_bbmd_address;

    bacnet_ctor(INTERNAL_FUNCTION_PARAM_PASSTHRU);
    z_ip_port = zend_read_property(bacnet_class_entry, getThis(), "port", sizeof("port") - 1, 1, &rv);
    z_bbmd_port = zend_read_property(bacnet_class_entry, getThis(), "bbmd_port", sizeof("bbmd_port") - 1, 1, &rv);
    z_bbmd_address = zend_read_property(bacnet_class_entry, getThis(), "bbmd_address", sizeof("bbmd_address") - 1, 1, &rv);
    
    bacnet_initialize(Z_STRVAL(*z_ip_port), Z_STRVAL(*z_bbmd_port), Z_STRVAL(*z_bbmd_address));
}

PHP_METHOD(Bacnet, whois)
{
    uint8_t result;
    char *argv[3];
    zend_long arg_v, arg1_v, arg2_v;
    zval whois_result;

    if( zend_parse_parameters( ZEND_NUM_ARGS(), "|sss", 
        &argv[0], &arg_v, &argv[1], &arg1_v, &argv[2], &arg2_v ) == FAILURE ) {
        RETURN_FALSE;
    }
    whois_initialize();
    result = whois(ZEND_NUM_ARGS(), argv);
    if(result != 0) {
        RETURN_FALSE;
    }
    get_whois_result(&whois_result);
    RETURN_ZVAL( &whois_result, 1, 1 );
}

/*
PHP_METHOD(Bacnet, epic)
{
    uint8_t result;
    char *argv[3];
    zend_long arg_v, arg1_v, arg2_v;
    zval epic_result;

    if( zend_parse_parameters( ZEND_NUM_ARGS(), "s|ss", 
        &argv[0], &arg_v, &argv[1], &arg1_v, &argv[2], &arg2_v ) == FAILURE ) {
        RETURN_FALSE;
    }
    epic_initialize();
    result = epic(ZEND_NUM_ARGS(), argv);
    if(result != 0) {
        RETURN_FALSE;
    }
    RETURN_TRUE;
//    get_whois_result(&whois_result);
//    RETURN_ZVAL( &whois_result, 1, 1 );
}
*/

/**
 *
        printf("Usage: %s device-instance object-type object-instance "
            "property [index]\r\n", filename_remove_path(argv[0]));
 *
 */
PHP_METHOD(Bacnet, readprop)
{
    uint8_t result;
    char *argv[5];
    zend_long arg_v, arg1_v, arg2_v, arg3_v, arg4_v;

    if( zend_parse_parameters( ZEND_NUM_ARGS(), "ssss|s",
        &argv[0], &arg_v, &argv[1], &arg1_v, &argv[2], &arg2_v, &argv[3], &arg3_v, &argv[4], &arg4_v ) == FAILURE ) {
        RETURN_FALSE;
    }

    readprop_initialize();
    result = readprop(ZEND_NUM_ARGS(), argv);
    if(result != 0) {
        RETURN_FALSE;
    }
    RETURN_TRUE;
}

PHP_METHOD(Bacnet, readrange)
{
    uint8_t result;
    char *argv[8];
    zend_long arg_v, arg1_v, arg2_v, arg3_v, arg4_v, arg5_v, arg6_v, arg7_v;

    if( zend_parse_parameters( ZEND_NUM_ARGS(), "ssss|ssss",
        &argv[0], &arg_v,  &argv[1], &arg1_v, &argv[2], &arg2_v, &argv[3], &arg3_v, &argv[4], &arg4_v,
        &argv[5], &arg5_v, &argv[6], &arg6_v, &argv[7], &arg7_v ) == FAILURE ) {
        RETURN_FALSE;
    }

    readrange_initialize();
    result = readrange(ZEND_NUM_ARGS(), argv);
    if(result != 0) {
        RETURN_FALSE;
    }
    RETURN_TRUE;
}

PHP_METHOD(Bacnet, readPropertyAckHandler)
{
    if( zend_parse_parameters( ZEND_NUM_ARGS(), "f", &readprop_ca, &readprop_ca_cache) == FAILURE ) {
        RETURN_FALSE;
    }

    RETURN_TRUE;
}

PHP_METHOD(Bacnet, readRangeAckHandler)
{
    if( zend_parse_parameters( ZEND_NUM_ARGS(), "f", &readrange_ca, &readrange_ca_cache) == FAILURE ) {
        RETURN_FALSE;
    }

    RETURN_TRUE;
}

PHP_METHOD(Bacnet, getLastErrorMsg)
{
    zend_string *error = get_Last_Error_Message();
    if(NULL == error) {
        error = strpprintf(0, "メッセージ未設定エラー");
    }
    RETURN_STR(error);
}

PHP_METHOD(Bacnet, getPropName)
{
    zend_long index;
    char *prop_name;
    zend_string *zs_prop_name;
    
    if( zend_parse_parameters( ZEND_NUM_ARGS(), "l", &index) == FAILURE ) {
        RETURN_FALSE;
    }

    prop_name = (char *)bactext_property_name(index);
    zs_prop_name = zend_string_init(prop_name, strlen(prop_name), 0);
    
    RETURN_STR(zs_prop_name);
}

PHP_METHOD(Bacnet, getPropIdFromName)
{
    zend_long index, arg_len;
    char *prop_name;
    
    if( zend_parse_parameters( ZEND_NUM_ARGS(), "s", &prop_name, &arg_len ) == FAILURE ) {
        RETURN_FALSE;
    }

    index = bactext_property_id(prop_name);
    RETURN_LONG(index);
}

PHP_METHOD(Bacnet, getObjectName)
{
    zend_long index;
    char *object_name;
    zend_string *zs_object_name;
    
    if( zend_parse_parameters( ZEND_NUM_ARGS(), "l", &index) == FAILURE ) {
        RETURN_FALSE;
    }

    object_name = (char *)bactext_object_type_name(index);
    zs_object_name = zend_string_init(object_name, strlen(object_name), 0);
    
    RETURN_STR(zs_object_name);
}

PHP_METHOD(Bacnet, getObjectTypeFromName)
{
    uint32_t index;
    zend_long arg_len;
    char *object_type_name;
    bool result = false;
    
    if( zend_parse_parameters( ZEND_NUM_ARGS(), "s", &object_type_name, &arg_len ) == FAILURE ) {
        RETURN_FALSE;
    }

    result = bactext_object_type_index(object_type_name, &index);
    if(!result) {
        RETURN_FALSE;
    }
    RETURN_LONG(index);
}

/** クラスメソッドを登録する */
const zend_function_entry bacnet_methods[] = {
    PHP_ME(Bacnet, __construct, arginfo_bacnet_create, ZEND_ACC_CTOR|ZEND_ACC_PUBLIC)
    PHP_ME(Bacnet, initialize, arginfo_bacnet_create, ZEND_ACC_PUBLIC)
    PHP_ME(Bacnet, whois, NULL, ZEND_ACC_PUBLIC)
    PHP_ME(Bacnet, readprop, NULL, ZEND_ACC_PUBLIC)
    PHP_ME(Bacnet, readrange, NULL, ZEND_ACC_PUBLIC)
    PHP_ME(Bacnet, getLastErrorMsg, NULL, ZEND_ACC_PUBLIC)

    PHP_ME(Bacnet, getPropName, NULL, ZEND_ACC_PUBLIC)
    PHP_ME(Bacnet, getPropIdFromName, NULL, ZEND_ACC_PUBLIC)
    PHP_ME(Bacnet, getObjectName, NULL, ZEND_ACC_PUBLIC)
    PHP_ME(Bacnet, getObjectTypeFromName, NULL, ZEND_ACC_PUBLIC)

    PHP_ME(Bacnet, readPropertyAckHandler, NULL, ZEND_ACC_PUBLIC)
    PHP_ME(Bacnet, readRangeAckHandler, NULL, ZEND_ACC_PUBLIC)

    PHP_FE_END	/* Must be the last line in bacnet_functions[] */
};

static void set_const_propaty(
    ABSTRACT_INFOMATION_ELEMENT *obj)
{
    char label[64] = {0};
    for(;;) {
        if(obj->index == -1) {
            break;
        }
        strcpy(label, obj->name);
        zend_declare_class_constant_long(bacnet_class_entry, label, strlen(label), obj->index);
        obj++;
    }
}

/* {{{ PHP_MINIT_FUNCTION
 */
PHP_MINIT_FUNCTION(bacnet)
{
    /* If you have INI entries, uncomment these lines
    REGISTER_INI_ENTRIES();
    */
    // classをZend構造体に登録する
    zend_class_entry ce;
    INIT_CLASS_ENTRY(ce, "Bacnet", bacnet_methods);
    bacnet_class_entry = zend_register_internal_class(&ce);

    // classプロパティ
    zend_declare_property_string(bacnet_class_entry, "port", sizeof("port") - 1, "", ZEND_ACC_PUBLIC);
    zend_declare_property_string(bacnet_class_entry, "bbmd_port", sizeof("bbmd_port") - 1, "", ZEND_ACC_PUBLIC);
    zend_declare_property_string(bacnet_class_entry, "bbmd_address", sizeof("bbmd_address") - 1, "", ZEND_ACC_PUBLIC);
    
    zend_declare_class_constant_long(bacnet_class_entry, "MAX_BACNET_PROPERTY_ID", strlen("MAX_BACNET_PROPERTY_ID"), MAX_BACNET_PROPERTY_ID);
    zend_declare_class_constant_long(bacnet_class_entry, "MAX_BACNET_OBJECT_TYPE", strlen("MAX_BACNET_OBJECT_TYPE"), MAX_BACNET_OBJECT_TYPE);
    set_const_propaty((ABSTRACT_INFOMATION_ELEMENT*)prop_information_list);
    set_const_propaty((ABSTRACT_INFOMATION_ELEMENT*)object_type_information_list);
    
    return SUCCESS;
}
/* }}} */

/* {{{ PHP_MSHUTDOWN_FUNCTION
 */
PHP_MSHUTDOWN_FUNCTION(bacnet)
{
	/* uncomment this line if you have INI entries
	UNREGISTER_INI_ENTRIES();
	*/
	return SUCCESS;
}
/* }}} */

/* Remove if there's nothing to do at request start */
/* {{{ PHP_RINIT_FUNCTION
 */
PHP_RINIT_FUNCTION(bacnet)
{
#if defined(COMPILE_DL_BACNET) && defined(ZTS)
	ZEND_TSRMLS_CACHE_UPDATE();
#endif
	return SUCCESS;
}
/* }}} */

/* Remove if there's nothing to do at request end */
/* {{{ PHP_RSHUTDOWN_FUNCTION
 */
PHP_RSHUTDOWN_FUNCTION(bacnet)
{
	return SUCCESS;
}
/* }}} */

/* {{{ PHP_MINFO_FUNCTION
 */
PHP_MINFO_FUNCTION(bacnet)
{
	php_info_print_table_start();
	php_info_print_table_header(2, "bacnet support", "enabled");
	php_info_print_table_header(2, "bacnet version", "1.0");
	php_info_print_table_end();

	/* Remove comments if you have entries in php.ini
	DISPLAY_INI_ENTRIES();
	*/
}
/* }}} */

/* {{{ bacnet_functions[]
 *
 * Every user visible function must have an entry in bacnet_functions[].
 */
const zend_function_entry bacnet_functions[] = {
    PHP_FE(bacnet_create, arginfo_bacnet_create)
    PHP_FE_END
};
/* }}} */


/* {{{ bacnet_module_entry
 */
zend_module_entry bacnet_module_entry = {
	STANDARD_MODULE_HEADER,
	"bacnet",
	bacnet_functions,
	PHP_MINIT(bacnet),
	PHP_MSHUTDOWN(bacnet),
	PHP_RINIT(bacnet),		/* Replace with NULL if there's nothing to do at request start */
	PHP_RSHUTDOWN(bacnet),	/* Replace with NULL if there's nothing to do at request end */
	PHP_MINFO(bacnet),
	PHP_BACNET_VERSION,
	STANDARD_MODULE_PROPERTIES
};
/* }}} */

#ifdef COMPILE_DL_BACNET
#ifdef ZTS
ZEND_TSRMLS_CACHE_DEFINE()
#endif
ZEND_GET_MODULE(bacnet)
#endif

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * End:
 * vim600: noet sw=4 ts=4 fdm=marker
 * vim<600: noet sw=4 ts=4
 */
