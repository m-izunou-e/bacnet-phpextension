dnl $Id$
dnl config.m4 for extension bacnet

dnl Comments in this file start with the string 'dnl'.
dnl Remove where necessary. This file will not work
dnl without editing.

dnl If your extension references something external, use with:

PHP_ARG_WITH(bacnet, for bacnet support,
Make sure that the comment is aligned:
[  --with-bacnet             Include bacnet support])

dnl Otherwise use enable:

dnl PHP_ARG_ENABLE(bacnet, whether to enable bacnet support,
dnl Make sure that the comment is aligned:
dnl [  --enable-bacnet           Enable bacnet support])

if test "$PHP_BACNET" != "no"; then
  dnl Write more examples of tests here...

  dnl # get library FOO build options from pkg-config output
  dnl AC_PATH_PROG(PKG_CONFIG, pkg-config, no)
  dnl AC_MSG_CHECKING(for libfoo)
  dnl if test -x "$PKG_CONFIG" && $PKG_CONFIG --exists foo; then
  dnl   if $PKG_CONFIG foo --atleast-version 1.2.3; then
  dnl     LIBFOO_CFLAGS=`$PKG_CONFIG foo --cflags`
  dnl     LIBFOO_LIBDIR=`$PKG_CONFIG foo --libs`
  dnl     LIBFOO_VERSON=`$PKG_CONFIG foo --modversion`
  dnl     AC_MSG_RESULT(from pkgconfig: version $LIBFOO_VERSON)
  dnl   else
  dnl     AC_MSG_ERROR(system libfoo is too old: version 1.2.3 required)
  dnl   fi
  dnl else
  dnl   AC_MSG_ERROR(pkg-config not found)
  dnl fi
  dnl PHP_EVAL_LIBLINE($LIBFOO_LIBDIR, BACNET_SHARED_LIBADD)
  dnl PHP_EVAL_INCLINE($LIBFOO_CFLAGS)

  dnl # --with-bacnet -> check with-path
  dnl SEARCH_PATH="/usr/local /usr"     # you might want to change this
  dnl SEARCH_FOR="/include/bacnet.h"  # you most likely want to change this
  dnl if test -r $PHP_BACNET/$SEARCH_FOR; then # path given as parameter
  dnl   BACNET_DIR=$PHP_BACNET
  dnl else # search default path list
  dnl   AC_MSG_CHECKING([for bacnet files in default path])
  dnl   for i in $SEARCH_PATH ; do
  dnl     if test -r $i/$SEARCH_FOR; then
  dnl       BACNET_DIR=$i
  dnl       AC_MSG_RESULT(found in $i)
  dnl     fi
  dnl   done
  dnl fi
  dnl
  dnl if test -z "$BACNET_DIR"; then
  dnl   AC_MSG_RESULT([not found])
  dnl   AC_MSG_ERROR([Please reinstall the bacnet distribution])
  dnl fi

  dnl # --with-bacnet -> add include path
  dnl PHP_ADD_INCLUDE($BACNET_DIR/include)

  dnl # --with-bacnet -> check for lib and symbol presence
  dnl LIBNAME=bacnet # you may want to change this
  dnl LIBSYMBOL=bacnet # you most likely want to change this 

  dnl PHP_CHECK_LIBRARY($LIBNAME,$LIBSYMBOL,
  dnl [
  dnl   PHP_ADD_LIBRARY_WITH_PATH($LIBNAME, $BACNET_DIR/$PHP_LIBDIR, BACNET_SHARED_LIBADD)
  dnl   AC_DEFINE(HAVE_BACNETLIB,1,[ ])
  dnl ],[
  dnl   AC_MSG_ERROR([wrong bacnet lib version or lib not found])
  dnl ],[
  dnl   -L$BACNET_DIR/$PHP_LIBDIR -lm
  dnl ])
  dnl
  dnl PHP_SUBST(BACNET_SHARED_LIBADD)

  BACNET_LIB_DIR=/home/mizunoue/bacnet-stack-086
  INCLUDE_DIR=$BACNET_LIB_DIR/include
  INCLUDE_PORTS_DIR=$BACNET_LIB_DIR/ports/linux
  PHP_LIBDIR=/home/mizunoue/php-dev/php-src/ext/bacnet

  PHP_ADD_INCLUDE($INCLUDE_PORTS_DIR)
  CFLAGS="$CFLAGS -I$INCLUDE_PORTS_DIR"

  PHP_ADD_INCLUDE($INCLUDE_DIR)
  PHP_ADD_INCLUDE($PHP_LIBDIR/include)
  PHP_ADD_INCLUDE($PHP_LIBDIR/include/object)

  LIBNAME=bacnet
dnl  PHP_ADD_LIBRARY($LIBNAME, BACNET_SHARED_LIBADD)
dnl  PHP_ADD_LIBRARY_WITH_PATH($LIBNAME, $BACNET_LIB_DIR/lib, BACNET_SHARED_LIBADD)
  PHP_ADD_LIBRARY_WITH_PATH($LIBNAME, /usr/lib64, BACNET_SHARED_LIBADD)
  AC_DEFINE(HAVE_LIBBACNET, 1, [ ])

  PHP_SUBST(BACNET_SHARED_LIBADD)

  PHP_NEW_EXTENSION(bacnet, bacnet.c src/bacnet_utility.c src/whois.c src/readprop.c src/readrange.c src/object/device-client.c, $ext_shared,, -DZEND_ENABLE_STATIC_TSRMLS_CACHE=1)

dnl  PHP_NEW_EXTENSION(bacnet, bacnet.c src/whois.c, $ext_shared,, -DZEND_ENABLE_STATIC_TSRMLS_CACHE=1)
fi
