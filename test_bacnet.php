<?php


/*
$obj = new Bacnet();

//$ref = new ReflectionClass( 'Bacnet' );
//var_dump($obj);

//var_dump($ref->getConstants());

var_dump($obj->getPropName(10));
var_dump($obj->getPropIdFromName('apdu-segment-timeout'));

var_dump($obj->getObjectName(14));
var_dump($obj->getObjectTypeFromName('multi-state-output'));

exit();
*/

class TestBACNet
{
    protected $bacnetInstance;

    public $deviceInstance = 1245;
//    public $deviceInstance = 112233;
    public $objectType;
    public $objectInstance;
    public $property;

    public function __construct($port = null, $bbmdPort = null, $bbmdAddress = null)
    {
        $this->bacnetInstance = new Bacnet($port, $bbmdPort, $bbmdAddress);
        $this->bacnetInstance->initialize();
//        $this->bacnetInstance = new Bacnet();
//        $this->bacnetInstance->initialize($port, $bbmdPort, $bbmdAddress);
    }
    
    public function initialize($port = null, $bbmdPort = null, $bbmdAddress = null)
    {
        $this->bacnetInstance->initialize($port, $bbmdPort, $bbmdAddress);
    }
    
    public function setDeviceInstance($di)
    {
        $this->deviceInstance = $di;
    }
    
    public function getBacnetInstance()
    {
        if(!$this->bacnetInstance) {
            $this->bacnetInstance =  new Bacnet();
        }
        return $this->bacnetInstance;
    }
    
    public function readPropetyHandler($arg)
    {
        print("--read propety--\n");
        var_dump($arg);
    }
    
    public function readRangeHandler($arg)
    {
        print("--read range--\n");
        var_dump($arg);
    }
    
    public function whois()
    {
        $res = $this->bacnetInstance->whois();
        var_dump($res);
    }
    
    public function readPropety()
    {
        $di = $this->deviceInstance;
        $ot = 8;
        $oi = $this->deviceInstance;
        $prop = 75;
//        $prop = 76;

        $ot = 1;
        $oi = 0;
        $prop = 85;

    
        $this->bacnetInstance->readPropertyAckHandler([$this, "readPropetyHandler"]);
        $res = $this->bacnetInstance->readprop($di, $ot, $oi, $prop);
        if(!$res) {
            var_dump($this->bacnetInstance->getLastErrorMsg());
        }
    }
    
    public function readRange()
    {
        $di = $this->deviceInstance;
        $ot = 20;
        $oi = 2;
        $prop = 131;
        $dt = 4;

        $this->bacnetInstance->readRangeAckHandler([$this, "readRangeHandler"]);
        $res = $this->bacnetInstance->readrange($di, $ot, $oi, $prop, $dt);
        if(!$res) {
            var_dump($this->bacnetInstance->getLastErrorMsg());
        }
    }
}

//$obj = new TestBACNet();
$obj1 = new TestBACNet(47809, 47808, '192.168.8.220');
$obj2 = new TestBACNet();
$obj2->setDeviceInstance(112233);

// whois
//$obj->whois();

// readprop
$obj1->readPropety();
$obj2->readPropety();

// readrange
$obj1->readRange();
$obj2->readRange();

