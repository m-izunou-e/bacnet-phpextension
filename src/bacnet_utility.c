#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>       /* for time */
#include <errno.h>

#include "php.h"
//#include "php_ini.h"
#include "zend_smart_str.h"

#define PRINT_ENABLED 1 // 仮。

#include "config.h" // bacnetのconfig.h。名前は変えたほうが混乱がなくてよいと思われる。
#include "bacdef.h"
#include "bacenum.h"
#include "bacapp.h"
#include "bactext.h"
#include "bacerror.h"
#include "address.h"
#include "rp.h"
#include "datalink.h"
#include "handlers.h"
#include "dlenv.h"

#include "include/object/device.h"


#include "include/bacnet_utility.h"

static BACNET_ADDRESS Target_Address;
static bool Error_Detected = false;
static FUNCTION_MODE function_mode = MODE_NONE;
static uint8_t Request_Invoke_ID = 0;
static zend_string *Last_Error_Message;

void set_Last_Error_Message(
    zend_string *msg)
{
    Last_Error_Message = msg;
}

zend_string* get_Last_Error_Message()
{
    return Last_Error_Message;
}

void whois_initialize()
{
    function_mode = MODE_WHOIS;
    reset_Error_Detected();
}

void readprop_initialize()
{
    set_Request_Invoke_ID(0);
    function_mode = MODE_READPROP;
    reset_Error_Detected();
}

void readrange_initialize()
{
    set_Request_Invoke_ID(0);
    function_mode = MODE_READRANGE;
    reset_Error_Detected();
}


void set_Error_Detected()
{
    Error_Detected = true;
}

void reset_Error_Detected()
{
    Error_Detected = false;
}

bool get_Error_Detected()
{
    return Error_Detected;
}

void set_Request_Invoke_ID(
    uint8_t invoke_id
)
{
    Request_Invoke_ID = invoke_id;
}

BACNET_ADDRESS* get_Target_Address()
{
    return &Target_Address;
}


/* Parse a string for a bacnet-address
**
** @return length of address parsed in bytes
*/
int parse_bac_address(
    BACNET_ADDRESS * dest,      /* [out] BACNET Address */
    char *src   /* [in] nul terminated string to parse */
    )
{
    int i = 0;
    uint16_t s;
    int a[4], p;
    int c = sscanf(src, "%u.%u.%u.%u:%u", &a[0], &a[1], &a[2], &a[3], &p);

    dest->len = 0;

    if (c == 1) {
        if (a[0] < 256) {       /* mstp */
            dest->adr[0] = a[0];
            dest->len = 1;
        } else if (a[0] < 0x0FFFF) {    /* lon */
            s = htons((uint16_t) a[0]);
            memcpy(&dest->adr[0], &s, 2);
            dest->len = 2;
        } else
            return 0;
    } else if (c == 5) {        /* ip address */
        for (i = 0; i < 4; i++) {
            if ((a[i] < 0) || (a[i] > 255)) {
                return 0;
            }
            dest->adr[i] = a[i];
        }
        s = htons((uint16_t) p);
        memcpy(&dest->adr[i], &s, 2);
        dest->len = 6;
    }
    return dest->len;
}


char* get_macaddr(
    uint8_t * addr,
    int len, bool is_print)
{
    char *result;
    smart_str macaddr = {0};
    char addr_parts[3];
    int j = 0;

    while (j < len) {
        if (j != 0) {
            if(is_print) {
                printf(":");
            }
            smart_str_appendl(&macaddr, ":", sizeof(":") - 1);
        }
        if(is_print) {
            printf("%02X", addr[j]);
        }
        sprintf(addr_parts, "%02X", addr[j]);
        smart_str_appendl(&macaddr, addr_parts, sizeof(addr_parts) - 1);
        j++;
    }
    if(is_print) {
        while (j < MAX_MAC_LEN) {
            printf("   ");
            j++;
        }
    }
    smart_str_0(&macaddr);
    result = ZSTR_VAL(macaddr.s);
    
    smart_str_free(&macaddr);
    
    return result;
}


void get_whois_result(
    zval *ret_addrlist )
{
    bool is_print = false;
    BACNET_ADDRESS address;
    char *net_addr;
    uint32_t device_id;
    unsigned max_apdu;
    uint8_t local_sadr = 0;

    zval ret_addr;
    array_init(ret_addrlist);

    int i;
    int addr_cnt = address_count();
    bool found = false;
    for( i = 0; i < addr_cnt; i++) {
        found = address_get_by_index(i, &device_id, &max_apdu, &address);
        if(!found) {
            continue;
        }
        array_init(&ret_addr);
        if (address.net) {
            net_addr = get_macaddr(address.adr, address.len, is_print);
        } else {
            net_addr = get_macaddr(&local_sadr, 1, is_print);
        }
        add_assoc_double(&ret_addr, "device_id", device_id);
        add_assoc_string(&ret_addr, "address", net_addr);
        add_assoc_string(&ret_addr, "macaddress", get_macaddr(address.mac, address.mac_len, is_print));
        add_assoc_double(&ret_addr, "address_net", address.net);
        add_assoc_double(&ret_addr, "max_apdu", max_apdu);
        add_index_zval(ret_addrlist, i, &ret_addr);
    }

}

static void setObjectType(
    BACNET_OBJECT_PROPERTY_VALUE * object_value,
    zval *objectType)
{
    BACNET_APPLICATION_DATA_VALUE *value;
    BACNET_PROPERTY_ID property = PROP_ALL;
    BACNET_OBJECT_TYPE object_type = MAX_BACNET_OBJECT_TYPE;
    
    array_init(objectType);
    if(object_value && object_value->value) {
        value = object_value->value;
        property = object_value->object_property;
        object_type = object_value->object_type;

        switch (value->tag) {
            case BACNET_APPLICATION_TAG_NULL:
                add_assoc_null(objectType, "type");
                break;
            case BACNET_APPLICATION_TAG_BOOLEAN:
                add_assoc_bool(objectType, "type", value->type.Boolean);
                break;
            case BACNET_APPLICATION_TAG_UNSIGNED_INT:
                add_assoc_long(objectType, "type", (unsigned long) value->type.Unsigned_Int);
                break;
            case BACNET_APPLICATION_TAG_SIGNED_INT:
                add_assoc_long(objectType, "type", (long) value->type.Signed_Int);
                break;
            case BACNET_APPLICATION_TAG_REAL:
                add_assoc_double(objectType, "type", (double) value->type.Real);
                break;
#if defined (BACAPP_DOUBLE)
            case BACNET_APPLICATION_TAG_DOUBLE:
                add_assoc_double(objectType, "type", (double) value->type.Double);
                break;
#endif
            case BACNET_APPLICATION_TAG_OCTET_STRING:
                add_assoc_string(objectType, "type", "OctetString");
//                add_assoc_string(objectType, "type", octetstring_value(&value->type.Octet_String));
                break;
            case BACNET_APPLICATION_TAG_CHARACTER_STRING:
                add_assoc_string(objectType, "type", characterstring_value(&value->type.Character_String));
                break;
            case BACNET_APPLICATION_TAG_BIT_STRING:
                add_assoc_string(objectType, "type", "BitString");
//                add_assoc_string(objectType, "type", bitstring_bit(&value->type.Bit_String));
                break;

            case BACNET_APPLICATION_TAG_ENUMERATED:
                add_assoc_long(objectType, "enum", (unsigned)value->type.Enumerated);
                add_assoc_long(objectType, "prop", (unsigned)property);
                switch (property) {
                    case PROP_OBJECT_TYPE:
                        if (value->type.Enumerated < MAX_ASHRAE_OBJECT_TYPE) {
                            add_assoc_string(objectType, "name", bactext_object_type_name(value->type.Enumerated));
                        } else if (value->type.Enumerated < 128) {
                            add_assoc_string(objectType, "name", "reserved");
                        } else {
                            add_assoc_string(objectType, "name", "proprietary");
                        }
                        break;
                    case PROP_EVENT_STATE:
                        add_assoc_string(objectType, "name", bactext_event_state_name(value->type.Enumerated));
                        break;
                    case PROP_UNITS:
                        if (value->type.Enumerated < 256) {
                            add_assoc_string(objectType, "name", bactext_engineering_unit_name(value->type.Enumerated));
                        } else {
                            add_assoc_string(objectType, "name", "proprietary");
                        }
                        break;
                    case PROP_POLARITY:
                        add_assoc_string(objectType, "name", bactext_binary_polarity_name(value->type.Enumerated));
                        break;
                    case PROP_PRESENT_VALUE:
                    case PROP_RELINQUISH_DEFAULT:
                        if (object_type < OBJECT_PROPRIETARY_MIN) {
                            add_assoc_string(objectType, "name", bactext_binary_present_value_name(value->type.Enumerated));
                        } else {
                        }
                        break;
                    case PROP_RELIABILITY:
                        add_assoc_string(objectType, "name", bactext_reliability_name(value->type.Enumerated));
                        break;
                    case PROP_SYSTEM_STATUS:
                        add_assoc_string(objectType, "name", bactext_device_status_name(value->type.Enumerated));
                        break;
                    case PROP_SEGMENTATION_SUPPORTED:
                        add_assoc_string(objectType, "name", bactext_segmentation_name(value->type.Enumerated));
                        break;
                    case PROP_NODE_TYPE:
                        add_assoc_string(objectType, "name", bactext_node_type_name(value->type.Enumerated));
                        break;
                    default:
                        break;
                }
                break;

            case BACNET_APPLICATION_TAG_DATE:
                add_assoc_long(objectType,   "year",  (unsigned)value->type.Date.year);
                add_assoc_string(objectType, "month", bactext_month_name(value->type.Date.month));
                add_assoc_long(objectType,   "day",   (unsigned)value->type.Date.day);
                add_assoc_string(objectType, "wday",  bactext_day_of_week_name(value->type.Date.wday));
                break;

            case BACNET_APPLICATION_TAG_TIME:
                add_assoc_long(objectType, "hour",       value->type.Time.hour);
                add_assoc_long(objectType, "min",        value->type.Time.min);
                add_assoc_long(objectType, "sec",        value->type.Time.sec);
                add_assoc_long(objectType, "hundredths", value->type.Time.hundredths);
                break;

            case BACNET_APPLICATION_TAG_OBJECT_ID:
                add_assoc_long(objectType, "type",     value->type.Object_Id.type);
                add_assoc_long(objectType, "instance", value->type.Object_Id.instance);
                if (value->type.Object_Id.type < MAX_ASHRAE_OBJECT_TYPE) {
                    add_assoc_string(objectType, "name", bactext_object_type_name(value->type.Object_Id.type));
                } else if (value->type.Object_Id.type < 128) {
                    add_assoc_string(objectType, "name", "reserved");
                } else {
                    add_assoc_string(objectType, "name", "proprietary");
                }
                break;
        }
    }
    
    return;
}

static bool bacapp_get_value(
    BACNET_OBJECT_PROPERTY_VALUE * object_value,
    zval *object)
{
    char *str;
    bool retval = false;
    size_t str_len = 32;
    int status;
    zval objectType;

    array_init(object);
    while (true) {
        /* Try to allocate memory for the output string. Give up if unable. */
        str = (char *) calloc(sizeof(char), str_len);
        if (!str)
            break;

        /* Try to extract the value into allocated memory. If unable, try again */
        /* another time with a string that is twice as large. */
        status = bacapp_snprintf_value(str, str_len, object_value);
        if ((status < 0) || (status >= str_len)) {
            free(str);
            str_len *= 2;
        } else if (status == 0) {
            free(str);
            break;
        } else {
            add_assoc_string(object, "value",       str);
            free(str);
            retval = true;
            break;
        }
    }
    
     add_assoc_long(object, "tag",  object_value->value->tag);
     setObjectType(object_value, &objectType);
     add_assoc_zval(object, "object", &objectType);
//            add_assoc_double(object, "context_tag", object_value->value->context_tag);
//            add_assoc_double(object, "array_index", object_value->array_index);
    
    
    return retval;
}

static void set_readprop_result(
    BACNET_READ_PROPERTY_DATA *data,
    zval *result)
{
    zval object;

    BACNET_OBJECT_PROPERTY_VALUE object_value;  // for bacapp printing
    BACNET_APPLICATION_DATA_VALUE value;        // for decode value data
    int len = 0;
    uint8_t *application_data;
    int application_data_len;

    int i = 0;
    array_init(result);

    if (data) {
        application_data = data->application_data;
        application_data_len = data->application_data_len;
        // FIXME: what if application_data_len is bigger than 255?
        // value? need to loop until all of the len is gone... 
        for (;;) {
            len =
                bacapp_decode_application_data(application_data,
                (uint8_t) application_data_len, &value);
            object_value.object_type = data->object_type;
            object_value.object_instance = data->object_instance;
            object_value.object_property = data->object_property;
            object_value.array_index = data->array_index;
            object_value.value = &value;

            bacapp_get_value(&object_value, &object);
            add_index_zval(result, i, &object);
            i++;

            if (len > 0) {
                if (len < application_data_len) {
                    application_data += len;
                    application_data_len -= len;
                    // there's more!
                } else {
                    break;
                }
            } else {
                break;
            }
        }
    }


/*
typedef struct BACnet_Application_Data_Value {
    bool context_specific;      // true if context specific data
    uint8_t context_tag;        // only used for context specific data
    uint8_t tag;        // application tag data type
    union {
        // NULL - not needed as it is encoded in the tag alone
#if defined (BACAPP_BOOLEAN)
        bool Boolean;
#endif
#if defined (BACAPP_UNSIGNED)
        uint32_t Unsigned_Int;
#endif
#if defined (BACAPP_SIGNED)
        int32_t Signed_Int;
#endif
#if defined (BACAPP_REAL)
        float Real;
#endif
#if defined (BACAPP_DOUBLE)
        double Double;
#endif
#if defined (BACAPP_OCTET_STRING)
        BACNET_OCTET_STRING Octet_String;
#endif
#if defined (BACAPP_CHARACTER_STRING)
        BACNET_CHARACTER_STRING Character_String;
#endif
#if defined (BACAPP_BIT_STRING)
        BACNET_BIT_STRING Bit_String;
#endif
#if defined (BACAPP_ENUMERATED)
        uint32_t Enumerated;
#endif
#if defined (BACAPP_DATE)
        BACNET_DATE Date;
#endif
#if defined (BACAPP_TIME)
        BACNET_TIME Time;
#endif
#if defined (BACAPP_OBJECT_ID)
        BACNET_OBJECT_ID Object_Id;
#endif
#if defined (BACAPP_LIGHTING_COMMAND)
        BACNET_LIGHTING_COMMAND Lighting_Command;
#endif
#if defined (BACAPP_DEVICE_OBJECT_PROP_REF)
        BACNET_DEVICE_OBJECT_PROPERTY_REFERENCE
            Device_Object_Property_Reference;
#endif
    } type;
    // simple linked list if needed
    struct BACnet_Application_Data_Value *next;
} BACNET_APPLICATION_DATA_VALUE;


typedef struct BACnet_Object_Property_Value {
    BACNET_OBJECT_TYPE object_type;
    uint32_t object_instance;
    BACNET_PROPERTY_ID object_property;
    uint32_t array_index;
    BACNET_APPLICATION_DATA_VALUE *value;
} BACNET_OBJECT_PROPERTY_VALUE;




*/


}

static void set_readrange_result(
    BACNET_READ_RANGE_DATA *data,
    zval *result)
{
    zval object;

    BACNET_OBJECT_PROPERTY_VALUE object_value;  // for bacapp printing
    BACNET_APPLICATION_DATA_VALUE value;        // for decode value data
    int len = 0;
    uint8_t *application_data;
    int application_data_len;

    int i = 0;
    array_init(result);

    if (data) {
        application_data = data->application_data;
        application_data_len = data->application_data_len;
        // FIXME: what if application_data_len is bigger than 255?
        // value? need to loop until all of the len is gone... 
        for (;;) {
            len =
                bacapp_decode_application_data(application_data,
                (uint8_t) application_data_len, &value);
            object_value.object_type = data->object_type;
            object_value.object_instance = data->object_instance;
            object_value.object_property = data->object_property;
            object_value.array_index = data->array_index;
            object_value.value = &value;

//            bacapp_print_value(stdout, &object_value);
            bacapp_get_value(&object_value, &object);
            add_index_zval(result, i, &object);
            i++;


            if (len > 0) {
                if (len < application_data_len) {
                    application_data += len;
                    application_data_len -= len;
                    // there's more!
                } else {
                    break;
                }
            } else {
                break;
            }
        }
    }
}

/**
 * Handler
 */

static void php_Read_Property_Ack_Handler(
    BACNET_READ_PROPERTY_DATA data)
{
    zval retval;
    zval arg;

    set_readprop_result(&data, &arg);

    readprop_ca.retval = &retval;
    readprop_ca.param_count = 1;
    readprop_ca.params = &arg;

    if(readprop_ca.size > 0) {
        zend_call_function(&readprop_ca, &readprop_ca_cache);
    }
    zval_ptr_dtor(&arg);
}

static void php_Read_Range_Ack_Handler(
    BACNET_READ_RANGE_DATA data)
{
    zval retval;
    zval arg;

    set_readrange_result(&data, &arg);

    readrange_ca.retval = &retval;
    readrange_ca.param_count = 1;
    readrange_ca.params = &arg;

    if(readrange_ca.size > 0) {
        zend_call_function(&readrange_ca, &readrange_ca_cache);
    }
    zval_ptr_dtor(&arg);
}

static void My_Read_Property_Ack_Handler(
    uint8_t * service_request,
    uint16_t service_len,
    BACNET_ADDRESS * src,
    BACNET_CONFIRMED_SERVICE_ACK_DATA * service_data)
{
    int len = 0;
    BACNET_READ_PROPERTY_DATA data;

    if (address_match(&Target_Address, src) &&
        (service_data->invoke_id == Request_Invoke_ID)) {
        len =
            rp_ack_decode_service_request(service_request, service_len, &data);
        if (len > 0) {
//            rp_ack_print_data(&data);
            php_Read_Property_Ack_Handler(data);
        }
    }
}
static void My_Read_Range_Ack_Handler(
    uint8_t * service_request,
    uint16_t service_len,
    BACNET_ADDRESS * src,
    BACNET_CONFIRMED_SERVICE_ACK_DATA * service_data)
{

//    handler_read_range_ack(service_request, service_len, src, service_data);
    int len = 0;
    BACNET_READ_RANGE_DATA data;


    (void) src;
    (void) service_data;
    len = rr_ack_decode_service_request(service_request, service_len, &data);

#if PRINT_ENABLED
    fprintf(stderr, "Received ReadRange Ack!\n");
#endif

    if (len > 0) {
        php_Read_Range_Ack_Handler(data);
    }
}


static void MyErrorHandler(
    BACNET_ADDRESS * src,
    uint8_t invoke_id,
    BACNET_ERROR_CLASS error_class,
    BACNET_ERROR_CODE error_code)
{
    zend_string *msg;
    if (address_match(&Target_Address, src) &&
        (invoke_id == Request_Invoke_ID)) {
        msg = strpprintf(0, "BACnet Error: %s: %s\r\n",
            bactext_error_class_name((int) error_class),
            bactext_error_code_name((int) error_code));
        set_Last_Error_Message(msg);
        Error_Detected = true;
    }
}


static void MyAbortHandler(
    BACNET_ADDRESS * src,
    uint8_t invoke_id,
    uint8_t abort_reason,
    bool server)
{
    (void) server;
    zend_string *msg;
    if(function_mode == MODE_READPROP || function_mode == MODE_READRANGE) {
        if (address_match(&Target_Address, src) &&
            (invoke_id == Request_Invoke_ID)) {
            msg = strpprintf(0, "BACnet Abort: %s\r\n",
                bactext_abort_reason_name((int) abort_reason));
            Error_Detected = true;
        }
    } else {
        /* FIXME: verify src and invoke id */
        (void) src;
        (void) invoke_id;
        msg = strpprintf(0, "BACnet Abort: %s\r\n",
            bactext_abort_reason_name(abort_reason));
        Error_Detected = true;
    }
    if( NULL != msg ) {
        set_Last_Error_Message(msg);
    }
}

static void MyRejectHandler(
    BACNET_ADDRESS * src,
    uint8_t invoke_id,
    uint8_t reject_reason)
{
    zend_string *msg;
    if(function_mode == MODE_READPROP || function_mode == MODE_READRANGE) {
        if (address_match(&Target_Address, src) &&
            (invoke_id == Request_Invoke_ID)) {
            msg = strpprintf(0, "BACnet Reject: %s\r\n",
                bactext_reject_reason_name((int) reject_reason));
            Error_Detected = true;
        }
    } else {
        /* FIXME: verify src and invoke id */
        (void) src;
        (void) invoke_id;
        msg = strpprintf(0, "BACnet Reject: %s\r\n",
            bactext_reject_reason_name(reject_reason));
        Error_Detected = true;
    }
    if( NULL != msg ) {
        set_Last_Error_Message(msg);
    }
}

static void MyWhoIsHandlerForRead(
    uint8_t * service_request,
    uint16_t service_len,
    BACNET_ADDRESS * src)
{
    if(function_mode == MODE_READPROP || function_mode == MODE_READRANGE) {
        handler_who_is(service_request, service_len, src);
    }
}


static void init_service_handlers(
    void)
{
    Device_Init(NULL);

    apdu_set_unconfirmed_handler(SERVICE_UNCONFIRMED_WHO_IS, MyWhoIsHandlerForRead);
    /* handle i-am to support binding to other devices */
    apdu_set_unconfirmed_handler(SERVICE_UNCONFIRMED_I_AM, handler_i_am_add);
//    apdu_set_unconfirmed_handler(SERVICE_UNCONFIRMED_I_AM, handler_i_am_bind);

    apdu_set_unrecognized_service_handler_handler(handler_unrecognized_service);

    apdu_set_confirmed_handler(SERVICE_CONFIRMED_READ_PROPERTY,
        handler_read_property);
    apdu_set_confirmed_ack_handler(SERVICE_CONFIRMED_READ_PROPERTY,
        My_Read_Property_Ack_Handler);
    apdu_set_confirmed_ack_handler(SERVICE_CONFIRMED_READ_RANGE,
        My_Read_Range_Ack_Handler);

    apdu_set_error_handler(SERVICE_CONFIRMED_READ_PROPERTY, MyErrorHandler);
    apdu_set_error_handler(SERVICE_CONFIRMED_PRIVATE_TRANSFER, MyErrorHandler);
    apdu_set_abort_handler(MyAbortHandler);
    apdu_set_reject_handler(MyRejectHandler);
}


/**
 * 初期化処理　共通で使えることを確認する
 */
void bacnet_initialize(
    char* ip_port,
    char* bbmd_port,
    char* bbmd_address)
{
    if(*ip_port) {
        setenv("BACNET_IP_PORT", ip_port, 1);
    }
    if(*bbmd_port) {
        setenv("BACNET_BBMD_PORT", bbmd_port, 1);
    }
    if(*bbmd_address) {
        setenv("BACNET_BBMD_ADDRESS", bbmd_address, 1);
    }

    Device_Set_Object_Instance_Number(BACNET_MAX_INSTANCE);
    init_service_handlers();
    address_init();
    dlenv_init();
    atexit(datalink_cleanup);
    
}


